import React                      from 'react';
import RunContent, { PlayerName } from '../RunContent';

const component = props => mount(<RunContent { ...props } />);
const IMG_URL   = 'https://www.speedrun.com/themes/esalpha/cover-256.png';

describe('<RunContent />', function(){

  it('Should render a span if error is set', function(){
    const props = {
      error : 'NO_RESULTS',
      run   : false,
      game  : { name : 'Test', imgLarge : IMG_URL }
    };
    expect(component(props).find('span')).toHaveLength(1);
  })

  it('Should render player name if run is ok', function(){
    const props = {
      error : false,
      run   : { playerName : 'carles', time : '0s', videoUri : IMG_URL },
      game  : { name : 'Test', imgLarge : IMG_URL }
    };
    expect(component(props).find(PlayerName)).toHaveLength(1);
  })
});