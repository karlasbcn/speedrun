import React, { Component } from 'react';
import styled               from 'styled-components';
import List                 from './List';
import RunContainer         from './RunContainer';
import { getGames }         from './api';

const Body = styled.div`
  @import url(https://fonts.googleapis.com/css?family=Lato:400,300);
  font-family: Lato;
  display: flex;
  flex-direction: column;
  overflow: hidden;
`

class App extends Component {
  state = {
    games        : null,
    error        : null,
    selectedGame : null
  };
  componentDidMount(){
    getGames()
      .then(games => this.setState({ games }))
      .catch(() => this.setState({ error : true }));
  }
  select(gameId){
    this.setState({ selectedGame : gameId });
  }
  render() {
    const { games, error, selectedGame } = this.state;
    if (error){
      return <span>Error requesting API</span>
    }
    if (!games){
      return <span>Loading...</span>
    }      
    return (
      <Body>
        { selectedGame && <RunContainer close={ () => this.select(null) } game={ games.find(game => game.id === selectedGame) } /> }
        <List games={ games } selectedGame={ selectedGame } select={ id => this.select(id) } />
      </Body>
    );
  }
}

export default App;
