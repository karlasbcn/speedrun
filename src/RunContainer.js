import React, { Component } from 'react';
import RunContent           from './RunContent';
import { getRun }           from './api';

class RunContainer extends Component {
  state = {
    run    : false,
    apiUri : null,
    error  : null,
  };
  static getDerivedStateFromProps(props, state) {
    const { apiUri } = props.game;
    if (apiUri !== state.apiUri) {
      return { apiUri, run : false }
    }
    return null;
  }
  componentDidMount(){
    this.fetchRun();
  }
  componentDidUpdate(){
    (typeof(this.state.run) === 'object') || this.fetchRun();
  }
  fetchRun(){
    getRun(this.state.apiUri)
      .then(run => this.setState({ 
        run, 
        error : run ? null : 'NO_RESULTS'
      }))
      .catch(() => this.setState({ 
        run   : false, 
        error : 'API_ERROR' 
      }));
  }
  render() {
    const { run, error }  = this.state;
    const { game, close } = this.props;
    return <RunContent game={ game } error={ error } run={ run } close={ close } />
  }
}

export default RunContainer;
