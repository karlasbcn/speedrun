import React    from 'react';
import styled   from 'styled-components';
import GameItem from './GameItem';

const Wrap = styled.div`
  display : flex;
  justify-content : space-between;
  flex-wrap: wrap;
  width : 100%;
`

const List = ({ games, selectedGame, select }) => {
  const gameItems = games.map(game => {
    const { id }     = game;
    const isSelected = selectedGame === id;
    return <GameItem key={ id } isSelected={ isSelected } select={ () => select(id) } game={ game } />;
  });
  return (
    <Wrap>
      { gameItems }
    </Wrap>
  );
}

export default List;
