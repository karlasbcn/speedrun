import React from 'react';
import styled   from 'styled-components';

const Wrap = styled.div`
  background-color: ${ ({ isSelected }) => isSelected ? 'green' : 'grey' };
  cursor: ${ ({ isSelected }) => isSelected ? 'default' : 'pointer' };
  color: white;
  height: 110px;
  width: 300px;
  margin: 12px;
  border-radius: 4px;
  display: flex;
  justify-content: space-between;
  transition: background-color .1s linear;
`

const Logo = styled.div`
  height: 72px;
  width: 72px;
  background-image: url(${ ({ src }) => src });
  background-size: auto;
  background-position: center;
  background-repeat: no-repeat;
  margin: 20px;
`;

const Name = styled.span`
  margin: 20px;
`;

const GameItem = ({ game, isSelected, select }) => (
  <Wrap isSelected={ isSelected } onClick={ select }>
    <Logo src={ game.imgSmall } />
    <Name>{ game.name }</Name>
  </Wrap>
)

export default GameItem;
