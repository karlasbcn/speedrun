import React, { Fragment } from 'react';
import styled              from 'styled-components';

const ERRORS = {
  API_ERROR  : 'There was an error requesting run to API',
  NO_RESULTS : 'There are no available runs for this game'
}

const Wrap = styled.div`
  height: 300px;
  width : 100%;
  background-color : ${ ({ error }) => error ? 'red' : 'black' };
  color: white;
  border-radius: 4px;
  display: flex;
  justify-content: space-between;
`

const Logo = styled.div`
  height: 256px;
  flex: 1;
  background-image: url(${ ({ src }) => src });
  background-size: auto;
  background-position: center;
  background-repeat: no-repeat;
  margin: 20px;
`

const Right = styled.div`
  flex: 3;
  display: flex;
  flex-direction: column;
  margin: 20px;
  & > *{
    margin-left: 20px;
  }
`

const GameName = styled.div`
  font-size: 42px;
  margin-bottom: 1em;
`;

export const PlayerName = styled.div`
  font-size: 32px;
  font-style: italic;
`;

const Time = styled.div`
  font-size: 32px;
  font-style: italic;
  color: yellow;
`;

const Video = styled.button`
  font-size: 32px;
  outline: none;
  background-color: green;
  border: none;
  margin-top: 1em;
  cursor: pointer;
  color: white;
`;

const Close = styled.div`
  cursor: pointer;
  position: absolute;
  right: 1em;
  top: 1em;
  &:hover{
    text-decoration: underline;
  }
`;

const RunInfo = ({ run, error }) => {
  if (error){
    return <span>{ ERRORS[error] }</span>;
  }
  if (typeof(run) !== 'object'){
    return <span>Loading...</span>;
  }
  return (
    <Fragment>
      <PlayerName>{ run.playerName }</PlayerName>
      <Time>{ run.time }</Time>
      <a href={ run.videoUri } target="_new">
        <Video>Watch video</Video>
      </a>
    </Fragment>
  )
}

const RunContent = ({ run, error, game, close }) => (
  <Wrap error={ error } >
    <Logo src={ game.imgLarge } />
    <Right>
      <GameName>{ game.name }</GameName>
      <RunInfo run={ run } error={ error } />
      <Close onClick={ close }>Close</Close>
    </Right>
  </Wrap>
)

export default RunContent;
