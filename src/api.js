import { get }  from 'axios';
import prettyMs from 'pretty-ms';

const gameParse = ({ id, links, assets, names }) => ({
  id,
  apiUri   : links.find(link => link.rel === 'runs').uri,
  name     : names.international,
  imgSmall : assets['cover-small'].uri,
  imgLarge : assets['cover-large'].uri
});

const runParse = ({ times, videos, playerData }) => ({
  time       : prettyMs(times.realtime_t * 1000),
  videoUri   : videos.links[0].uri,
  playerName : playerData.name || playerData.names.international
});

const getUrl = url => new Promise(function(resolve, reject){
  get(url).then(response => resolve(response.data.data)).catch(reject);
});

export const getGames = () => new Promise(function(resolve, reject){
  getUrl('https://www.speedrun.com/api/v1/games')
    .then(games => resolve(games.map(gameParse)))
    .catch(reject);
});

export const getRun = uri => new Promise(function(resolve, reject){
  getUrl(uri).then(function(runs){
    if (runs && runs.length){
      const run = runs[0];
      getUrl(run.players[0].uri)
        .then(playerData => playerData ? resolve(runParse({ ...run, playerData })) : reject())
        .catch(reject);
    }
    else{
      resolve();
    }
  })
  .catch(reject);
});





export default getUrl;