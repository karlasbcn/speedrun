# Speedrun test

Done using React, and bootstraped with ``create-react-app``. 

Since app is quite simple, there was no need to use Redux. Other used relevant libraries are ``axios`` for API requests, and ``styled-components`` for styling.


Clone this repo and run following commands to play:

```sh
cd speedrun
<npm / yarn> install
<npm / yarn> start
```

Running tests:
```sh
<npm / yarn> test
```

